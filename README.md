# ics-ans-role-traefik

Ansible role to install traefik.

[Traefik](https://docs.traefik.io) is a modern HTTP reverse proxy and load balancer
made to deploy microservices with ease.

This role configures traefik with HTTP redirect to HTTPS, Let's Encrypt support
and Docker backend.

For traefik to redirect traffic to your docker container, you should:

- add your container to the `traefik_network` network
- expose the port to use
- set the following labels:
    * traefik.enable: "true"
    * traefik.backend: "{name}"
    * traefik.port: {port}
    * traefik.frontend.rule: "Host:{host}"
    * traefik.docker.network: "{traefik_network}"

The `traefik.frontend.rule` is set to `Host:{containerName}.{domain}` by default.
It's important to set the `traefik.docker.network` if your container is linked to several networks.
Otherwise it will randomly pick one.
The `traefik.port` label is not needed if you expose only one port. See the [traefik.toml documentation](https://docs.traefik.io/toml/#docker-backend)
for more information.

Here is an example using the [docker_container](https://docs.ansible.com/ansible/docker_container_module.html)
module from Ansible:

```yaml
- name: launch my container
  docker_container:
    name: mycontainer
    ...
    purge_networks: yes
    networks:
      - name: "{{traefik_network}}"
    labels:
      traefik.enable: "true"
      traefik.backend: "mycontainer"
      traefik.port: "{{mycontainer_port | int}}"
      traefik.frontend.rule: "Host:mycontainer.example.com"
      traefik.docker.network: "{{traefik_network}}"
```

## Requirements

- ansible >= 2.7
- molecule >= 2.20

## Role Variables

```yaml
traefik_version: 1.6
traefik_network: traefik-network
traefik_debug: "false"
traefik_send_anonymous_usage: "false"
# Accepted values, in order of severity: "DEBUG", "INFO", "WARN", "ERROR", "FATAL", "PANIC"
traefik_log_level: ERROR
# Dashboard only available from localhost by default
# Set to 8080 if you want to access it from another machine
traefik_dashboard_port: 127.0.0.1:8080
traefik_use_letsencrypt: false
traefik_domain: esss.lu.se
traefik_letsencrypt_email: ""
# Use the let's encrypt staging server by default
# Set to true to use the production environment
traefik_letsencrypt_production: false
traefik_cert_file: "{{ certificate_cert_chain_path }}"
traefik_key_file: "{{ certificate_key_path }}"
```

To enable let's encrypt, set `traefik_use_letsencrypt` to true.
By default, ics-ans-role-certificate will be used to populate `traefik_cert_file` and `traefik_key_file`.

The traefik dashboard is only available from localhost by default.
Set `traefik_dashboard_port` to 8080 if you want to access it from another machine.

This role uses the let's encrypt staging server by default.
Set `traefik_letsencrypt_production` to true to use the production environment.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-traefik
```

## License

BSD 2-clause
